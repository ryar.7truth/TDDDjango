$(document).ready(function() {
    $('.my-select').select2();
	    $(".chat-text").keypress(function(e) {
    //untuk cek key yang ditekan Enter atau bukan
    if(e.which == 13) {
    	e.preventDefault(); //default event tombol enter tidak dilakukan
    	var input = $("textarea").val(); //ambil data di textarea
      $("textarea").val(""); //menghapus text area
      $(".msg-insert").append('<p class="msg-send">'+input+'</p>'+'<br/>'); //menampilkan text di body chat
    }
	
	});
	
	//ketika buttonnya yang di-click
	$(".chat-button").click(function(){
      var input = $("textarea").val(); //ambil data di textarea
      $("textarea").val(""); //menghapus text area
        $(".msg-insert").append('<p class="msg-send">'+input+'</p>'+'<br/>'); //menampilkan text di body chat
    });

//inisiasi data tema
  var themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
  var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

  //untuk apply default theme, dijalankan saat web pertama kali dibuka
  if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  }
  //untuk simpan data tema di local storage
  localStorage.setItem('themes', JSON.stringify(themes));

  //untuk memuat tema ke select2
  var retrievedObject = localStorage.getItem('themes');
  $('.my-select').select2({'data': JSON.parse(retrievedObject)})

  //untuk menerapkan selectedTheme yang ada di local storage
  var retrievedSelected = JSON.parse(localStorage.getItem('selectedTheme'));
  var key;
  var bcgColor;
  var fontColor;
  for (key in retrievedSelected) {
  	if (retrievedSelected.hasOwnProperty(key)) {
       	bcgColor=retrievedSelected[key].bcgColor;
       	fontColor=retrievedSelected[key].fontColor;
   	}
  }  
  $("body").css({"background-color": bcgColor});
  $("footer").css({"color":fontColor});
	
	
	//implementasi fungsi tombol apply
	$('.apply-button').on('click', function(){  // sesuaikan class button
    var valueTheme = $('.my-select').val();
    var theme;
    var a;
    var selectedTheme = {};
    //mencari tema yang sesuai dengan id
    for(a in themes){
    	if(a==valueTheme){
    		var bcgColor = themes[a].bcgColor;
    		var fontColor = themes[a].fontColor;
    		var text = themes[a].text;
    		$("body").css({"background-color": bcgColor});
			  $("footer").css({"color":fontColor});
    		selectedTheme[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
    		localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
    	}
    }
});


});


// Calculator
var erase = false;

var go = function(x) {
  var print = document.getElementById('print');

  if (x === 'ac') {
    /* implemetnasi clear all */
      print.value = "";
      erase = false;
  }
  else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }
  else if (x==='log'||x==='sin'||x==='tan') {
      if(x==='sin'||x==='tan'){
          print.value = eval('Math.'+ x +'('+toDegrees(print.value)+')').toPrecision(2);
      }
      else {
  	  print.value = eval('Math.'+ x +'10'+'('+print.value+')').toPrecision(2);
      }
      erase = true;
  }
  else {
	if (erase === true){
	    print.value = "";
	    erase = false;
	}
	else {
            print.value + = x;
	    erase = false;
	}
  }
}

function toDegrees (angle) {
  return angle * Math.PI/180;
}

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

