$(document).ready(function(){

  localStorage.setItem('theme', JSON.stringify(theme));
	var localTheme = localStorage.getItem('theme');
  if(localStorage.getItem('selectedTheme')==null){
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));  
  }
  var localSelected = localStorage.getItem('selectedTheme');
	
  $('.my-select').select2({ 
    'data': JSON.parse(localTheme)
  });

 $(".chat-text").keypress(function(e) {
    //cek apakah key yang ditekan tombol Enter atau bukan
    if(e.which == 13) {
    	e.preventDefault(); //default event tombol enter tidak dilakukan
    	var input = $("textarea").val(); //ambil data di textarea
      $("textarea").val(""); //menghapus text area
      $(".msg-insert").append('<p class="msg-send">'+input+'</p>'+'<br/>'); //munculkan text di body chat
    }
	});
  
  apply(JSON.parse(localStorage.getItem('selectedTheme')));

	$('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var selected = $('.my-select').val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    var dict = theme[selected];
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    apply(dict);
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    localStorage.setItem('selectedTheme', JSON.stringify(dict));
	});

});

function apply(theme){
  $("body").css("background", theme.bcgColor);
  $(".theme").css("color", theme.fontColor);  
  $("footer").css("color", theme.fontColor);  
}

var theme = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];

var selectedTheme = {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"};    

var print = document.getElementById('print');
var erase = false;
var expand = true;
$('.msg-insert').hide();


// Calculator
var erase = false;

var go = function(x) {
  var print = document.getElementById('print');

  if (x === 'ac') {
    /* implemetnasi clear all */
      print.value = "";
      erase = false;
  }
  else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }
  else if (x==='log'||x==='sin'||x==='tan') {
      if(x==='sin'||x==='tan'){
          print.value = eval('Math.'+ x +'('+toDegrees(print.value)+')').toPrecision(2);
      }
      else {
  	  print.value = eval('Math.'+ x +'10'+'('+print.value+')').toPrecision(2);
      }
      erase = true;
  }
  else {
	if (erase === true){
	    print.value = x;
	    erase = false;
	}
	else {
            print.value+=x;
	    erase = false;
	}
  }
}

function toDegrees (angle) {
  return angle * Math.PI/180;
}

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

