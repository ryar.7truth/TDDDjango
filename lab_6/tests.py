from lab_1.views import mhs_name
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.

class Lab4UnitTest(TestCase):
    def test_lab_6_url_is_exist(self):
        response=Client().get('/lab-6/')
        self.assertEqual(response.status_code, 200)
